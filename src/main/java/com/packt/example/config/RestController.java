package com.packt.example.config;

import java.security.Principal;

import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@org.springframework.web.bind.annotation.RestController
@RequestMapping("/rest/hello")
public class RestController {
	
	@GetMapping
	public String hello() {
		return "Hello World";
	}
	
	@GetMapping("/principal")
	public Principal user(Principal principal) {
		return principal;
	}

}
